<?php

namespace App\Jobs;

use App\Organization;
use App\Proxy;
use App\Services\GosuParser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HouseGraberThread implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    public $timeout = 259200;

    /**
     * @var \App\Proxy
     */
    private $proxy;

    /**
     * @var int
     */
    private int $org_from;

    /**
     * @var int
     */
    private int $org_to;
    /**
     * @var \App\Services\GosuParser
     */
    private GosuParser $parser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($proxy, $org_from, $org_to)
    {
        $this->proxy = $proxy;
        $this->org_from = $org_from;
        $this->org_to = $org_to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->proxy = Proxy::find($this->proxy);
        $this->parser = new GosuParser($this->proxy->getConfig());
        while (true) {
            $organizations = Organization::whereNull('status')
                ->where('id', '>=', $this->org_from)
                ->where('id', '<=', $this->org_to)
                ->limit(100)->get();
            if (count($organizations) < 1)
                return;
//            ->chunk(100, function ($organizations) {
            /** @var Organization[] $organizations */
            foreach ($organizations as $organization) {
                if (!$organization->isServiceCompany()) {
                    $organization->update(['status' => 3]);
                    continue;
                }
                try {
                    $houses = $this->parser->getOrganizationHouses($organization);
                } catch (\Exception $exception) {
                    continue;
                }
                foreach ($houses as $house) {
                    try {
                        $houseInfo = $this->parser->getHouseInfo($house->guid);
                        $house->data = $houseInfo;
                        $house->status = 2;
                    } catch (\Exception $exception) {
//                            dd($house, $exception);
                    }
                    $house->save();
                    $this->proxy->ping();
                    usleep(1700 * 1000);
                }
                $organization->update(['status' => 2]);
                usleep(1700 * 1000);
            }
//    });
        }
    }
}
