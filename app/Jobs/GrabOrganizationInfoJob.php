<?php

namespace App\Jobs;

use App\Organization;
use App\Region;
use App\Services\GosuParser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Helper\Table;

class GrabOrganizationInfoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $guid;

    /**
     * @var GosuParser
     */
    private static $parser;

    /**
     * @var \Illuminate\Support\Collection|\App\Region[]
     */
    private static $regions;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($guid)
    {
        $this->guid = $guid;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        Organization::create([
            'guid' => $this->guid,
            'name' => 'new',
        ]);
//        try {
//            if (!static::$parser)
//                static::$parser = new GosuParser();
//            if (!static::$regions)
//                static::$regions = Region::get(['id', 'guid'])->pluck('id', 'guid');
//
//            $info = static::$parser->getOrganizationInfo($this->guid);
//            Organization::firstOrCreate([
//                'guid' => $info->guid,
//            ], [
//                'name' => $info->fullName,
//                'region_id' => static::$regions[$info->factualAddress->region->guid],
//                'guid' => $info->guid,
//                'root_guid' => $info->registryOrganizationRootEntityGuid,
//                'inn' => $info->inn,
//                'kpp' => $info->kpp,
//                'ogrn' => $info->ogrn,
//                'data' => $info,
//            ]);
//        } catch (\Exception $exception) {
//            Log::channel('parsing')->error($exception->getMessage());
////            Log::channel('parsing')->error($exception->getMessage());
//        }
//        sleep(2);
    }
}
