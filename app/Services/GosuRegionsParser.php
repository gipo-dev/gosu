<?php


namespace App\Services;


use App\Region;
use GuzzleHttp\Client;

class GosuRegionsParser
{
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function regions()
    {
        return json_decode($this->client->get('https://dom.gosuslugi.ru/nsi/api/rest/services/nsi/fias/v4/regions')->getBody()->getContents());
    }

    /**
     * @param \App\Region $region
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cities(Region $region)
    {
        return json_decode($this->client->get("https://dom.gosuslugi.ru/nsi/api/rest/services/nsi/fias/v4/cities?actual=true&itemsPerPage=1000&page=1&regionCode=$region->code&searchString=")->getBody()->getContents());
    }

    /**
     * @param \App\Region $region
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function settlements(Region $region)
    {
        return json_decode($this->client->get("https://dom.gosuslugi.ru/nsi/api/rest/services/nsi/fias/v4/settlements?actual=true&itemsPerPage=1000&page=1&regionCode=$region->code&searchString=")->getBody()->getContents());
    }
}
