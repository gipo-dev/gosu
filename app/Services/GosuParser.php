<?php

namespace App\Services;

use App\House;
use App\Organization;
use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Object_;

class GosuParser
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var mixed|null
     */
    private $region = null;

    /**
     * @var \App\Services\GosuRegionsParser
     */
    public $regions;

    /**
     * GosuParser constructor.
     */
    public function __construct($client_config = [])
    {
        $this->client = new Client($client_config);
        $this->regions = new GosuRegionsParser();
    }

    /**
     * @param null $region
     * @return \App\Services\GosuParser
     */
    public function setRegion($region = null)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param $page
     * @return array
     */
    public function getOrganizationsList($page)
    {
        $resp = $this->post("https://dom.gosuslugi.ru/ppa/api/rest/services/ppa/public/organizations/searchByOrg?pageIndex=$page&elementsPerPage=500", [
            'organizationRoleList' => [],
            'factualAddress' => [
                'area' => NULL,
                'city' => NULL,
                'region' =>
                    [
                        'guid' => $this->region->guid ?? NULL,
                        'code' => $this->region->code ?? NULL,
                    ],
                'settlement' => NULL,
                'street' => NULL,
                'planningStructureElement' => NULL,
                'house' => NULL,
                'houseNumber' => NULL,
                'buildingNumber' => NULL,
                'structNumber' => NULL,
            ],
            'organizationStatuses' => [
                'coll' => [
                    0 => 'REGISTERED',
                ],
                'operand' => 'OR',
            ],
            'okopfList' => [],
            'roleStatuses' => [0 => 'APPROVED'],
            'onlyHeadOrganization' => false,
        ]);
        return json_decode($resp->getBody()->getContents())->organizationSummaryWithNsiList;
    }

    /**
     * @param $guid
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOrganizationInfo($guid)
    {
        $info = json_decode($this->client->get("https://dom.gosuslugi.ru/ppa/api/rest/services/ppa/public/organizations/orgByGuid?organizationGuid=$guid")->getBody()->getContents());
        $info->additionalInfo = json_decode($this->post("https://dom.gosuslugi.ru/ppa/api/rest/services/ppa/public/organizations/additionalinfo", [
            'organizationGuids' => [$guid],
        ])->getBody()->getContents());
        return $info;
    }

    public function getOrganizationHouses(Organization $organization)
    {
        $houses = [];
        for ($page = 1; $page < 200; $page++) {
            usleep(1700 * 1000);
            $data = json_decode($this->post("https://dom.gosuslugi.ru/agreements/api/rest/services/agreements/public/objects/search;page=$page;itemsPerPage=500", [
                'archived' => false,
                'fullAddressSearchCriteria' => new Object_(),
                'orgRootGuid' => $organization->guid,
                'elementsPerPage' => 500,
                'pageIndex' => $page,
            ])->getBody()->getContents());
            if (count($data) < 1)
                break;
            foreach ($data as $item) {
                try {
                    $houses[] = new House([
                        'guid' => $item->houseHMGuid ?? null,
                        'name' => $item->fiasHouseAddress->formattedAddress ?? '',
                        'organization_id' => $organization->id,
                        'data' => $item,
                        'status' => 1,
                    ]);
                } catch (\Exception $exception) {
//                    dd($exception, $item);
                }
            }
        }
        return $houses;
    }

    /**
     * @param $guid
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHouseInfo($guid)
    {
        return json_decode($this->client->get("https://dom.gosuslugi.ru/homemanagement/api/rest/services/houses/public/1/$guid")->getBody()->getContents());
    }

    private function post($url, $json)
    {
        return $this->client->post($url, [
            'headers' => [
                'Accept' => 'application/json; charset=utf-8',
                'Content-Type' => 'application/json;charset=UTF-8',
            ],
            'json' => $json
        ]);
    }
}
