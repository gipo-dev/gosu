<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return array
     */
    public function getConfig()
    {
        if ($this->conn == 'local')
            return [];
        return [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko)'
            ],
            'timeout' => 10,
            'proxy' => $this->conn,
        ];
    }

    public function ping()
    {
        $this->update(['ping' => now()]);
    }
}
