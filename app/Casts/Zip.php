<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Zip implements CastsAttributes
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $key
     * @param mixed $value
     * @param array $attributes
     * @return mixed|void
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return json_decode(gzuncompress($value));
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $key
     * @param mixed $value
     * @param array $attributes
     * @return false|mixed|string
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return gzcompress(json_encode($value), 9);
    }
}
