<?php

namespace App\Commands;

use App\House;
use App\Jobs\HouseGraberThread;
use App\Organization;
use App\Proxy;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class GrabHousesManager extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'houses:manager {operation}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Менеджер управления потоками парсинга домов';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->{$this->argument('operation')}();
    }

    /**
     * Запускает сбор домов
     */
    private function start()
    {
        $i = 0;
        $proxy = Proxy::get();
        $query = Organization::select(['id'])->whereNull('status');
        $part_size = ceil($query->count() / $proxy->count());
        $this->info('part size is ' . $part_size);
        $query->chunk($part_size, function ($organizations) use (&$i, $proxy) {
            $this->info('task added ' . $i);
            HouseGraberThread::dispatch($proxy[$i++]->id, $organizations->min('id'), $organizations->max('id'));
        });
    }

    /**
     * Отключает компании, которые не являются управляющими организациями
     */
    private function prepare()
    {
        $query = Organization::whereNull('status');
        $pb = $this->output->createProgressBar($query->count());
        $query->chunk(300, function ($organizations) use (&$pb) {
            foreach ($organizations as $organization) {
                /** @var Organization $organization */
                if (!$organization->isServiceCompany())
                    $organization->update(['status' => 3]);
                $pb->advance();
            }
        });
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
