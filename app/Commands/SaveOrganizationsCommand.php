<?php

namespace App\Commands;

use App\Organization;
use App\Region;
use App\Services\GosuParser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use LaravelZero\Framework\Commands\Command;

class SaveOrganizationsCommand extends Command
{
    /**
     * @var GosuParser[]
     */
    private static $parsers = [];

    /**
     * @var array
     */
    private static $regions;

    private static int $parsers_count;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'org:save';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Переносит организации из задач в бд';

    public function __construct()
    {
        parent::__construct();

        static::$parsers[] = new GosuParser();
        static::$parsers[] = new GosuParser(['proxy' => 'http://vxYpsspq:LmphxMwC@45.95.28.96:54989']);
        static::$parsers[] = new GosuParser(['proxy' => 'http://vxYpsspq:LmphxMwC@45.95.28.93:48271']);
        static::$parsers[] = new GosuParser(['proxy' => 'http://vxYpsspq:LmphxMwC@45.152.116.106:45294']);

        static::$parsers_count = count(static::$parsers);

        static::$regions = Region::get(['id', 'guid'])->pluck('id', 'guid');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Organization::where('name', 'new')->chunk(100, function ($organizations) {
            foreach ($organizations as $i => $organization) {
                try {
                    $info = $this->getParser()->getOrganizationInfo($organization->guid);
                    $organization->update([
                        'name' => $info->fullName,
                        'region_id' => static::$regions[$info->factualAddress->region->guid],
                        'guid' => $info->guid,
                        'root_guid' => $info->registryOrganizationRootEntityGuid,
                        'inn' => $info->inn,
                        'kpp' => $info->kpp,
                        'ogrn' => $info->ogrn,
                        'data' => $info,
                    ]);
                    $this->info('Сохранено ' . ($i + 1) . '/100');
                } catch (\Exception $exception) {
                    $this->info('Ошибка ' . $exception->getMessage());
                    Log::channel('parsing')->error($exception->getMessage());
                }
                usleep((1700 / static::$parsers_count) * 1000);
            }
        });
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    private $last_parser = 0;

    private function getParser()
    {
        if (++$this->last_parser >= static::$parsers_count)
            $this->last_parser = 0;
        return static::$parsers[$this->last_parser];
    }
}
