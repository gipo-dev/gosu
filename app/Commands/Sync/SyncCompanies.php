<?php

namespace App\Commands\Sync;

use App\Organization;
use App\Production\Company;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class SyncCompanies extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'companies:sync';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Синхронизирует управляющие компании';


    public function handle()
    {
        $total = 0;
        Company::whereNull('sync_id')->chunkById(1000, function ($companies) use (&$total) {
            $organizations = Organization::whereIn('inn', $companies->pluck('inn'))->get()->keyBy('inn');
            $companies->each(function (Company $company) use (&$organizations, &$total) {
                $local = $organizations[$company->inn] ?? false;
                if ($local) {
                    try {
                        $company->update(['sync_id' => $local->id]);
                        $this->info('Привязано: ' . $company->id);
                    } catch (\Exception $exception) {
                    }
                } else {
                    $total++;
                    $this->error('Не найдено: ' . $company->inn);
                }
            });
        });
        $this->error('Не найдено: ' . $total);
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
