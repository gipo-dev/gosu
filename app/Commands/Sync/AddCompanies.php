<?php

namespace App\Commands\Sync;

use App\Organization;
use App\Production\Company;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class AddCompanies extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'companies:add';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Добавляет новые компании';

    /**
     * @var string[]
     */
    private $allowedCompanyTypes = ['УО', 'УК', 'ТСЖ', 'ТСН', 'ЖСК'];

    /**
     * @var
     */
    private $exists;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->exists = Company::whereNotNull('sync_id')->get(['sync_id'])->pluck('sync_id', 'sync_id');
        Organization::whereNotNull('data')->with(['region'])
            ->chunkById(1000, function ($locals, $index) {
                foreach ($locals as $local) {
                    if (isset($this->exists[$local->id])) {
                        $this->info('Уже существует');
                        $this->updateCompany($local, $this->exists[$local->id]);
                    } else {
                        $this->createCompany($local);
                    }
                }
            });
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    /**
     * @param $data
     * @return string
     */
    private function getScheduling($data)
    {
        $schedule = [];
        foreach ($data->additionalInfo->additionalInfos as $additional) {
            if (isset($additional->organizationOpeningHours)) {
                foreach ($additional->organizationOpeningHours as $day) {
                    $days = ['Воскресенье: ', 'Понедельник: ', 'Вторник: ', 'Среда: ', 'Четверг: ', 'Пятница: ', 'Суббота: '];
                    $item = '';
                    if ($day->openHours->beginDate) {
                        $date = Carbon::parse($day->openHours->beginDate);
                        $item .= 'с ' . $date->hour . ':' . $date->format('i');
                    }
                    if ($day->openHours->endDate) {
                        $date = Carbon::parse($day->openHours->endDate);
                        $item .= ' до ' . $date->hour . ':' . $date->format('i');
                    }
                    if (!$day->openHours->beginDate && !$day->openHours->endDate)
                        $item = 'Выходной';

                    if ($day->breakHours->beginDate) {
                        $date = Carbon::parse($day->breakHours->beginDate);
                        $item .= ', перерыв с ' . $date->hour . ':' . $date->format('i');
                    }
                    if ($day->breakHours->endDate) {
                        $date = Carbon::parse($day->breakHours->endDate);
                        $item .= ' до ' . $date->hour . ':' . $date->format('i');
                    }
                    if (trim($item) == '')
                        $item .= 'Не указано';
                    else
                        $item = $days[$day->dayOfWeek] . $item;

                    $schedule[$day->dayOfWeek > 0 ? $day->dayOfWeek : 7] = $item;
                }
            }
        }
        ksort($schedule, SORT_NATURAL);
        $schedule = implode(PHP_EOL, $schedule);
        if (trim($schedule) == '')
            $schedule = 'Не указано';
        return $schedule;
    }

    /**
     * @param \App\Organization $local
     */
    private function createCompany(Organization $local)
    {
        try {
            $region = $local->region;
            $roles = collect($local->data->organizationRoles)->whereIn('role.shortName', $this->allowedCompanyTypes);
            if ($roles->count() < 1) {
                $this->info('0 ролей');
                return;
            }
            $roleName = $roles->first()->role->organizationRoleName;
            $additionalInfos = collect($local->data->additionalInfo->additionalInfos);
            $city = $additionalInfos[0]->controlLocation->city->formalName ?? $local->data->additionalInfo->additionalInfos[0]->controlLocation->settlement->formalName ?? $local->data->factualAddress->city->formalName ?? $local->data->factualAddress->area->formalName ?? $roles->first()->oktmo->name;
            $fullName = explode('"', $local->data->fullName);
            $fields = [
                'region' => $region->name,
                'regioncode' => $region->data->regionCode,
                'regionlinkname' => Str::slug($region->name),
                'address' => $local->data->factualAddress->formattedAddress,
                'localityname' => $city,
                'localitylinkname' => Str::slug($city),
                'lat' => null,
                'lng' => null,
                'shortname' => $local->data->shortName,
                'fullname' => $local->data->fullName,
                'inn' => $local->data->inn,
                'ogrn' => $local->data->ogrn,
                'email' => $local->data->orgEmail,
                'phones' => $local->data->phone,
                'legalform' => $local->data->okopf->name,
                'postaddress' => $local->data->postalAddress->formattedAddress,
                'regaddress' => $local->data->orgAddress,
                'site' => $local->data->url,
                'openinghours' => $this->getScheduling($local->data),
                'headname' => $additionalInfos->first() && $additionalInfos->first()->chiefInfo ? $additionalInfos->first()->chiefInfo->fio : '',
                'housescount' => null,
                'housessquare' => null,
                'employees' => null,
                'housespopulation' => null,
                'facthousescount' => '-1',
                'cleanname' => preg_replace('/[^ a-zа-яё\d]/ui', '', $local->data->shortName),
                'nameprefix' => $roleName,
                'fshortname' => $local->data->shortName,
                'ffullname' => isset($fullName[1]) ? ($roleName . ' ' . $fullName[1]) : $local->data->fullName,
            ];
            $company = Company::create($fields);
            $this->exists[$company->id] = $company->id;
            $company->update(['sync_id' => $local->id]);
            $this->info($local->id);
        } catch (\Exception $exception) {
            $this->error($local->id . ': ' . $exception->getMessage());
        }
    }

    private function updateCompany(Organization $local, $remote)
    {
        try {
            $region = $local->region;
            $roles = collect($local->data->organizationRoles)->whereIn('role.shortName', $this->allowedCompanyTypes);
            if ($roles->count() < 1) {
                $this->info('0 ролей');
                return;
            }
            $roleName = $roles->first()->role->organizationRoleName;
            $additionalInfos = collect($local->data->additionalInfo->additionalInfos);
            $city = $additionalInfos[0]->controlLocation->city->formalName ?? $local->data->additionalInfo->additionalInfos[0]->controlLocation->settlement->formalName ?? $local->data->factualAddress->city->formalName ?? $local->data->factualAddress->area->formalName ?? $roles->first()->oktmo->name;
            $fullName = explode('"', $local->data->fullName);
            Company::where('sync_id', $remote)
                ->update([
                    'region' => $region->name,
                    'regioncode' => $region->data->regionCode,
                    'address' => $local->data->factualAddress->formattedAddress,
                    'localityname' => $city,
                    'shortname' => $local->data->shortName,
                    'fullname' => $local->data->fullName,
                    'ogrn' => $local->data->ogrn,
                    'email' => $local->data->orgEmail,
                    'phones' => $local->data->phone,
                    'legalform' => $local->data->okopf->name,
                    'postaddress' => $local->data->postalAddress->formattedAddress,
                    'regaddress' => $local->data->orgAddress,
                    'site' => $local->data->url,
                    'openinghours' => $this->getScheduling($local->data),
                    'headname' => $additionalInfos->first() && $additionalInfos->first()->chiefInfo ? $additionalInfos->first()->chiefInfo->fio : '',
                    'cleanname' => preg_replace('/[^ a-zа-яё\d]/ui', '', $local->data->shortName),
                    'nameprefix' => $roleName,
                    'fshortname' => $local->data->shortName,
                    'ffullname' => isset($fullName[1]) ? ($roleName . ' ' . $fullName[1]) : $local->data->fullName,
                ]);
        } catch (\Exception $exception) {
            $this->error($local->id . ': ' . $exception->getMessage());
        }
    }
}
