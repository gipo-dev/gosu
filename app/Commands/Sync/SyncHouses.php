<?php

namespace App\Commands\Sync;

use App\House;
use App\RemoteHouse;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use LaravelZero\Framework\Commands\Command;

class SyncHouses extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'houses:sync';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Синхронизация существующих домов';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exists = collect([]);
        RemoteHouse::select('sync_id')->whereNotNull('sync_id')->chunk(50000, function ($houses) use (&$exists) {
            $exists = $exists->merge($houses->pluck('sync_id'));
        });
        $exists = $exists->toArray();
        $sync = 0;
        $this->info('Запуск');
        House::whereNotNull('data')->chunkById(10000, function (Collection $houses, $thousand) use (&$sync, &$exists) {
            $houses->each(function (House $house, $index) use (&$sync, $thousand, &$exists) {
                try {
                    if(in_array($house->id, $exists))
                        return;
                    $data = $house->data;
                    if (!isset($data->address))
                        return;
                    $remote = RemoteHouse::where('regioncode', $data->address->region->regionCode)
                        ->where('city', $data->address->city->formalName ?? $data->address->settlement->formalName ?? $data->address->region->formalName)
                        ->where('streetwithouttype', $data->address->street->formalName ?? $data->address->settlement->formalName)
                        ->where('housenum', $data->address->house->houseNumber)
                        ->first(['id']);
                    if (is_null($remote))
                        return;
                    try {
                        RemoteHouse::where('id', $remote->id)->update([
                            'sync_id' => $house->id,
                        ]);
                        $this->info('Синхронизировано: ' . ++$sync . '/' . (($thousand - 1) * 10000 + $index));
                    } catch (\Exception $exception) {
                    }
                } catch (\Exception $exception) {
//                    dd($exception, $house);
                    $this->error($exception->getMessage());
                }
            });
        });
        dd(123);
        // city->shortName . '. ' . city->formalName . ', ' . street->shortName . '. ' . street->formalName . ', д. ' . house->houseNumber
        //регион + город + улица + номер дома
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
