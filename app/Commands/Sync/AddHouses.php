<?php

namespace App\Commands\Sync;

use App\House;
use App\RemoteHouse;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Collection;
use LaravelZero\Framework\Commands\Command;

class AddHouses extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'houses:add';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Добавляет и обновляет дома';

    /**
     * @var int[]
     */
    private $exists;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        dd(House::first()->data);
//        $this->exists = RemoteHouse::whereNotNull('sync_id')->get(['sync_id'])->pluck('sync_id', 'sync_id');

        House::whereNotNull('data')->chunk(1000, function (Collection $houses, $thousand) use (&$sync) {
            $houses->each(function (House $house, $index) use (&$sync, $thousand) {
                if (isset($this->exists[$house->id]))
                    $this->updateHouse($house);
                else
                    $this->createHouse($house);
            });
        });
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    /**
     * @param \App\House $house
     */
    private function createHouse(House $house)
    {
        $data = $house->data;

        dd($data);
//        RemoteHouse::create
        $remote = ([
            'sync_id' => $house->id,
            'sourceurl' => '',
            'address' => $this->formatAddress($data),
            'square' => $data->buildingSquare ?? 'Не заполнено',
            'managestartdate' => $data->totalSquare ?? 'Не заполнено',
//            'companyid' =>
        ]);
        dd($remote);
    }

    private function formatAddress($data)
    {
        $address = '';
        if ($data->address->area) {
            $address .= $data->address->area->shortName . '. ' . $data->address->area->formalName . ', ';
        }
        if ($data->address->city) {
            $address .= $data->address->city->shortName . '. ' . $data->address->city->formalName . ', ';
        }
        if ($data->address->settlement) {
            $address .= $data->address->settlement->shortName . '. ' . $data->address->settlement->formalName . ', ';
        }
        $address .= $data->address->street->shortName . '. ' . $data->address->street->formalName . ', д. ' . $data->address->house->houseNumber;
        return $address;
    }
}
