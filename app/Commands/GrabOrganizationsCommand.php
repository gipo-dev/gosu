<?php

namespace App\Commands;

use App\Jobs\GrabOrganizationInfoJob;
use App\Region;
use App\Services\GosuParser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use LaravelZero\Framework\Commands\Command;

class GrabOrganizationsCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'org:grab {--page=1}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Сохраняет список всех организаций';
    /**
     * @var \App\Services\GosuParser
     */
    private $parser;
    private $page;

    private function init()
    {
        $this->parser = new GosuParser();
        $this->page = (int)$this->option('page');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();
        $page = 1;
        while (count($orgs = $this->parser
//                ->setRegion(Region::find('65'))
                ->getOrganizationsList($page)) > 0) {
            foreach ($orgs as $org) {
                dispatch(new GrabOrganizationInfoJob($org->guid));
            }
            $this->info($page * 500);
            Log::channel('parser')->info('Выполнено ' . ($page * 500));
            $page++;
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
