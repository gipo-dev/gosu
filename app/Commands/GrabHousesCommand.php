<?php

namespace App\Commands;

use App\Organization;
use App\Services\GosuParser;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class GrabHousesCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'houses:grab';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Парсит дома по организациям';
    /**
     * @var \App\Services\GosuParser
     */
    private GosuParser $parser;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parser = new GosuParser();
        Organization::select(['id', 'guid'])
            ->whereNull('status')
            ->orderBy('id')
            ->chunk(500, function ($organizations) {
                foreach ($organizations as $i => $organization) {
                    $houses = $this->parser->getOrganizationHouses($organization);
                    if (count($houses) > 0) {
                        $organization->saveMany($houses);
                        $this->info(count($houses) . ' домов');
                    } else
                        $this->info('0 домов');
                    $organization->update(['status' => 1]);
                    $this->info($i);
                }
            });
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
