<?php

namespace App\Commands;

use App\City;
use App\Region;
use App\Services\GosuRegionsParser;
use App\Settlement;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class GrabCitiesCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'cities:grab';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var \App\Services\GosuRegionsParser
     */
    private $parser;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->parser = new GosuRegionsParser();
        $this->parseRegions();
        $this->parseCities();
        $this->parseSettlements();
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function parseRegions()
    {
        foreach ($this->parser->regions() as $region) {
            Region::firstOrCreate([
                'guid' => $region->guid,
            ], [
                'name' => ($region->shortName ?? '') . ' ' . ($region->formalName ?? ''),
                'code' => $region->aoGuid,
                'data' => $region,
            ]);
        }
    }

    private function parseCities()
    {
        $regions = Region::get(['id', 'code']);
        foreach ($regions as $region) {
            $cities = $this->parser->cities($region);
            foreach ($cities as $city) {
                City::firstOrCreate([
                    'guid' => $city->guid,
                ], [
                    'name' => $city->formalName,
                    'region_id' => $region->id,
                    'parent_guid' => $city->parentGuid,
                    'data' => $city,
                ]);
            }
        }
    }

    private function parseSettlements()
    {
        $regions = Region::get(['id', 'code']);
        foreach ($regions as $region) {
            $settlements = $this->parser->settlements($region);
            foreach ($settlements as $settlement) {
                Settlement::firstOrCreate([
                    'guid' => $settlement->guid,
                ], [
                    'name' => $settlement->shortName . '. ' . $settlement->formalName,
                    'region_id' => $region->id,
                    'parent_guid' => $settlement->parentGuid,
                    'data' => $settlement,
                ]);
            }
        }
    }
}
