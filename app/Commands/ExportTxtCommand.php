<?php

namespace App\Commands;

use App\Organization;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class ExportTxtCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'export:txt';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Экпортирует первые 100 организаций в txt-файл';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $organizations = json_encode(Organization::limit(20)->get()->map(function (&$org) {
            return $org->toArray();
        })->toArray());
//        dd($organizations);
        file_put_contents('organizations.txt', print_r($organizations, true));
        dd('ok');
//        header("Content-Disposition: attachment; filename=\"demo.xls\"");
//        header("Content-Type: application/vnd.ms-excel;");
//        header("Pragma: no-cache");
//        header("Expires: 0");
//        $out = fopen("php://output", 'w');
//        foreach ($organizations as $data)
//        {
//            $data['data'] = json_encode($data['data']), true;
////            dd($data);
////            fputcsv($out, $data,"\t");
//        }
//        fclose($out);
    }
}
