<?php

namespace App;

use App\Casts\Zip;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class House
 * @package App
 *
 * @property int $id
 * @property string $guid
 * @property string $name
 * @property int $city_id
 * @property int $organization_id
 * @property object $data
 * @property int $status
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 */
class House extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'data' => Zip::class,
    ];
}
